<?php

function crude_menus_create_menu() {
	if( ! isset( $_POST[ 'nonce' ] ) || ! wp_verify_nonce( $_POST[ 'nonce' ], 'cm_create_menu_nonce') ) {
		$text = __( 'Your request has timed out.  Please refresh this page and try again.', 'crude-menus' );
		$response = crude_menus_create_admin_notice( 'expired_nonce', 'notice-success', $text );
		print wp_json_encode( $response ); die();
	}

	$full_menu_array = array();
	$menu_title = isset( $_POST[ 'title' ] ) ? sanitize_text_field( $_POST[ 'title' ] ) : '';
	$menu_data = isset( $_POST[ 'menuData' ] ) ? sanitize_textarea_field( $_POST[ 'menuData' ] ): '';

	if( ! $menu_title || ! $menu_data ) {
		$text = __( 'Please ensure that you have filled out both fields.', 'crude-menus' );
		$response = crude_menus_create_admin_notice( 'missing_data', 'notice-error', $text );
		print wp_json_encode( $response ); die();
	}

	// The menu title might have escaped characters, so remove backticks
	// This is really just one backtick, but it's escaped. Très ironique, no?
	$menu_title = str_replace( '\\', '', $menu_title ); 

	$menu_exists = wp_get_nav_menu_object( $menu_title );

	if( $menu_exists ) {
		$text = __( 'A menu named "' . $menu_title . '" already exists.  Please change it below and try again.', 'crude-menus' );
		$response = crude_menus_create_admin_notice( 'menu_exists', 'notice-error', $text );
		print wp_json_encode( $response ); die();
	}

	// This will create an array of rows
	$menu_array = explode( PHP_EOL, $menu_data );

	/*
	 * These rows might have properties like CSS class or link target,
	 * as a comma-separated list.  Explode each row into a separate array
	 * of properties for that new link.
	 */

	foreach( $menu_array as $row ) {
		// Replace ', ' with ',' to avoid spaces in the elements
		$row = str_replace( ', ', ',', $row );

		// Make an array of the row, splitting at commas
		$row_array = explode( ',', $row );

		// Add the array to our master array
		$full_menu_array[] = $row_array;
	}

	/*
	 * If we've gotten to this point, everything checks out.  There's a menu
	 * name (which doesn't already exist) and there's menu content.  So, let's
	 * create the menu, then assign the links to it.
	 */

	$new_menu_id = wp_create_nav_menu( $menu_title );

	foreach( $full_menu_array as $menu_item ) {
		$link = $menu_item[ 0 ];
		$title = $menu_item[ 1 ];
		$title = wp_slash( $menu_item[ 1 ] );
		$css_class = isset( $menu_item[ 2 ] ) ? $menu_item[ 2 ] : '';
		$link_target = isset( $menu_item[ 3 ] ) ? '_blank' : '';

		wp_update_nav_menu_item( $new_menu_id, 0, array(
			'menu-item-type'	=> 'custom',
			'menu-item-url'		=> $link,
			'menu-item-title'	=> $title,
			'menu-item-target'	=> $link_target,
			'menu-item-classes'	=> $css_class,
			'menu-item-status'	=> 'publish',
		) );
	}

	// Send a success message back through the AJAX tubes
	$text = __( 'The "' . $menu_title . '" menu has been created!', 'crude-menus' );
	$response = crude_menus_create_admin_notice( 'menu_created', 'notice-success', $text );
	print wp_json_encode( $response ); die();

}

add_action( 'wp_ajax_crude_menus_create_menu', 'crude_menus_create_menu' );

// Helper function to create admin notices

function crude_menus_create_admin_notice( $key, $class, $text ) {
	$out = array(
		'key' 		=> $key,
		'markup'	=> '',
	);

	$markup = '';
	$markup .= '<div class="cm-notice notice ' . $class . '">';
		$markup .= '<p>' . $text . '</p>';
	$markup .= '</div>';

	$out[ 'markup' ] = $markup;

	return $out;
}