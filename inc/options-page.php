<?php

// Add a submenu as part of the Appearance menu

function cm_add_crude_menus_page() { 
	add_theme_page(
		'CRUDE Menus', 					// Page title
		'CRUDE Menus', 					// Menu title
		'manage_options',				// Cap needed
		'crude-menus',					// Menu slug
		'cm_create_menu_form_output'	// Callback function
	);
}

add_action( 'admin_menu', 'cm_add_crude_menus_page' );

/*
 * Since this plugin isn't writing to the wp_options table, I'm electing to
 * roll my own form, rather than using the Settings API.  Validation will
 * happen in create-menu.php, which handles the POST submission.
 */

function cm_create_menu_form_output() {
	wp_enqueue_script( 'cm_ajax' );
	wp_localize_script( 'cm_ajax', 'cm_menu_settings', array(
		'nonce'		=> wp_create_nonce( 'cm_create_menu_nonce' ),
	) );

	$form_action_url = esc_url( admin_url( 'admin-post.php' ) );

	// Grab the title from $_GET if it's present, to prepopulate the field
	$title_value = isset( $_GET[ 'cm_menu_name' ] ) ? urldecode( $_GET[ 'cm_menu_name' ] ) : '';

	// We don't need the title pre-populated if the menu was created successfully
	if( isset( $_GET[ 'cm_admin_notice' ] ) && $_GET[ 'cm_admin_notice' ] == 'success' ) {
		$title_value = ''; 
	}

	// Easier to set the text here so as not to clutter the HTML
	$textareaplacehodler = 'Link, Title, CSS Class, Link Target' . PHP_EOL . 'Link, Title, CSS Class, Link Target...';
	?>

	<h1>CRUDE Menus - Create Menu</h1>
	<p>You can create a new menu by simply pasting a list of links and page titles into the box below.</p>
	<p>Your list should have only one menu item per row.  These rows can be comma-separated values, with the following options:</p>
	<ol>
		<li>Link (required)</li>
		<li>Title (required)</li>
		<li>Extra CSS Class</li>
		<li>Should this menu item open in a new tab? If left blank, it will open in the same tab; any other value will set it to open in a new tab.</li>
	</ol>
	<p>Note: only the Link and Title are required; the other two are optional.</p>
	<p>All links will be added as Custom Links to the new menu.</p>
	<form id="cm-create-form" method="post" action="<?php echo $form_action_url; ?>">
		<input type="hidden" name="action" value="cm_create_menu_form" />
		<label for="cm-create-menu-title">Menu Title</label><br />
		<input type="text" class="cm-text-input" id="cm-create-menu-title" name="cm_create_menu_title" value="<?php echo $title_value; ?>"/><br /><br />
		<label for="cm-create-menu-content"">Menu Content</label><br />
		<textarea placeholder="<?php echo $textareaplacehodler; ?>" cols="60" rows="10" type="text" class="cm-textarea" id="cm-create-menu-content" name="cm_create_menu_content" /></textarea><br /><br />
		<input type="submit" value="Create Menu" />
	</form>
<?php }